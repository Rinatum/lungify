# Lungify
 
A framework to support a medical data science project in a production environment.

## System design

![System Design](./system_design.png)

## Project Structure
```bash
.
├── adapter                                     # Dicom adapter implemented using Orthanc PACS.
│   ├── dicom/rest.py                           # src code for python callbacks
│   └── docker
│       ├── .env 
│       ├── docker-compose.yaml
│       ├── Dockerfile
│       ├── orthanc.json                        # main Orthanc config
│       └── requirements.txt 
├── models                                      # all AI models used in service
│   ├── classification_pathology_saliency       # lung pathology classification
│   ├── monochrome_classifier 
│   └── orientation_classifier
└── workflow                                    # airflow based pipeline
    ├── dags                                    # all airflow dags here
    ├── docker
    └── plugins                                 # additional python modules
        ├── dicom_                              # some useful modules for working with dicom
        ├── transforms                          # interface for transform implementation and some useful transforms here
        ├── triton                              # triton client implementation, used to communicate with models
        └── utils 
```

## Triton Inference Server

All models and their configs are stored in specific directory. 
Triton serves this models and allows to access them in different ways. 

### Run 

Run model using official docker image nvcr.io/nvidia/tritonserver:20.07-py3

* -e NVIDIA_VISIBLE_DEVICES=0 `# Use only gpu 0 for inference.`
* --ipc=host `# Use the host system’s IPC namespace.`
* -p4000:8000 `# Access http using external port 4000`
* PATH_TO_MODELS_DIR `# path to your models directory`

```
nvidia-docker run --name triton_models -e NVIDIA_VISIBLE_DEVICES=0 --ipc=host -p4000:8000 -v <PATH_TO_MODELS_DIR>:/models nvcr.io/nvidia/tritonserver:20.07-py3 tritonserver --model-store=/models
```
