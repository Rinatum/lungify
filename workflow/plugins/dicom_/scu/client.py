from pydicom import read_file
from pynetdicom import AE
from pynetdicom.sop_class import ComprehensiveSRStorage, SecondaryCaptureImageStorage


class DicomClient:
    def __init__(self):
        self.ae = AE()
        self.ae.add_requested_context(ComprehensiveSRStorage)
        self.ae.add_requested_context(SecondaryCaptureImageStorage)

    def store(self, host, port, dataset_path, ae=b'ANY-SCP'):
        dataset = read_file(dataset_path)

        assoc = self.ae.associate(host, port, ae_title=ae)
        if assoc.is_established:
            # Use the C-STORE service to send the dataset
            # returns the response status as a pydicom Dataset
            status = assoc.send_c_store(dataset)

            # Check the status of the storage request
            if status:
                # If the storage request succeeded this will be 0x0000
                print('C-STORE request status: 0x{0:04x}'.format(status.Status))
            else:
                print('Connection timed out, was aborted or received invalid response')

            # Release the association
            assoc.release()
        else:
            print('Association rejected, aborted or never connected')