import logging
import os
import subprocess

logger = logging.getLogger("airflow.task")


class DicomService:
    def __init__(self, dicom_ip: str, dicom_port: int, dicom_ae_title: str):
        self.dicom_ip = dicom_ip
        self.dicom_port = dicom_port
        self.dicom_ae_title = dicom_ae_title
        #
        # self.store_scp = kwargs.get('store_scp')
        # self.trust_store = kwargs.get('trust_store')
        # self.trust_store_pass = kwargs.get('trust_store_pass')
        # self.dest_ip = kwargs.get('dest_ip')
        # self.dest_port = kwargs.get('dest_port')

    def move(self, study_id):
        raise NotImplementedError

    def store(self, dicom_file_path):
        raise NotImplementedError


class CliAE(DicomService):
    def __init__(self, dicom_ip: str, dicom_port: int, dicom_ae_title: str,
                 ssl: bool = False,
                 trust_store: str = None, trust_store_pass: str = None):
        super().__init__(dicom_ip, dicom_port, dicom_ae_title,
 )
        self.ssl = ssl
        self.trust_store = trust_store
        self.trust_store_pass = trust_store_pass

    def store(self, dicom_file_path):
        ssl_context = " " if not self.ssl else \
            f"--tls " \
                f"--ssl2Hello " \
                f"--trust-store {self.trust_store} " \
                f"--trust-store-pass {self.trust_store_pass} "

        command = [
            f"storescu -c {self.dicom_ae_title}@{self.dicom_ip}:{self.dicom_port} {ssl_context} {dicom_file_path}"
        ]

        logger.info(command)

        output = subprocess.check_output(command,
                                         shell=True,
                                         stderr=subprocess.STDOUT,
                                         timeout=60,
                                         universal_newlines=True)
        return output

