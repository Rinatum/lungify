import os
import pydicom
from pydicom import Dataset
from dicom_.reader.base import CascadeOfDicomFilters, DicomDirFilter


class ViewPositionFilter(DicomDirFilter):
    @staticmethod
    def check(dicom_meta: Dataset):
        print("check view position")
        return ("ViewPosition" in dicom_meta and dicom_meta.ViewPosition in {"AP", "PA", ""}) \
               or "ViewPosition" not in dicom_meta


class TomographyFilter(DicomDirFilter):
    @staticmethod
    def check(dicom_meta: Dataset):
        print("check tomography")
        return "ScanOptions" not in dicom_meta and "TomoType" not in dicom_meta \
               and "TomoClass" not in dicom_meta


class ModalityFilter(DicomDirFilter):
    @staticmethod
    def check(dicom_meta: Dataset):
        print("check modality")
        return "Modality" in dicom_meta and \
               (dicom_meta.Modality == "DX" or dicom_meta.Modality == "RF" or dicom_meta.Modality == "CR")


def filter_lung_dicom_dir(dicom_dir_path):
    return filter_dicom_dir(dicom_dir_path, CascadeOfDicomFilters((
        ViewPositionFilter(), TomographyFilter(), ModalityFilter()
    )))


def filter_dicom_dir(dicom_dir_path, filter_func):
    dicom_paths = []
    for input_dicom_path in os.listdir(dicom_dir_path):
        full_path = os.path.join(dicom_dir_path, input_dicom_path)
        dicom_dataset = pydicom.read_file(full_path, stop_before_pixels=True)

        if filter_func(dicom_dataset):
            dicom_paths.append(full_path)

    return dicom_paths
