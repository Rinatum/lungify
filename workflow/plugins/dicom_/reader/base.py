from collections.abc import Callable
from pydicom import Dataset
from typing import List


class DicomDirFilter(Callable):
    def __call__(self, dicom_meta: Dataset):
        return self.check(dicom_meta)

    @staticmethod
    def check(dicom_meta: Dataset):
        return NotImplementedError


class CascadeOfDicomFilters(Callable):
    def __init__(self, filters: List[DicomDirFilter]):
        self.filters = filters

    def __call__(self, dicom_meta: Dataset):
        passed = True
        for dicom_filter in self.filters:
            print("Filtering: ", type(dicom_filter), dicom_filter(dicom_meta))
            passed = passed and dicom_filter(dicom_meta)
        return passed
