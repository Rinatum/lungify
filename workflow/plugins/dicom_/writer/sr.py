import os

from dicom_.writer.reports import generate_basic_dataset, generate_sequence_report, create_file_meta


class SR:
    def __init__(self, origin_ds, proba, series_number="2", THRESHOLD=0.5):
        self.original_UID = origin_ds.SeriesInstanceUID
        ds = generate_basic_dataset(add_id=series_number, origin_ds=origin_ds, original_UID=self.original_UID)

        # HEADER BEGIN
        ds.Modality = "SR"
        ds.SOPClassUID = "1.2.840.10008.5.1.4.1.1.88.33"

        age = origin_ds.get("PatientAge")
        if age:
            ds.PatientAge = age
        # HEADER END

        content_sequence = generate_sequence_report(origin_ds, proba, THRESHOLD)

        ds.ContentSequence = content_sequence
        ds.file_meta = create_file_meta(ds.SOPInstanceUID)
        ds.is_implicit_VR = True
        ds.is_little_endian = True
        ds.SpecificCharacterSet = 'ISO_IR 192'
        self.ds = ds

    def save(self, output_dir, name=None):
        if not name:
            name = self.ds.SOPInstanceUID
        # Write to the output directory and add the extension dcm, to force writing in DICOM format.
        dicom_filename = "{0}.dcm".format(str(name))
        dicom_path = os.path.join(output_dir, dicom_filename)
        self.ds.save_as(dicom_path, write_like_original=False)

        return dicom_path
