import datetime
import pytz

from pydicom.uid import UID as uid
from pydicom.filewriter import multi_string

MAX_SERIES_LENGTH = 63

def generate_UID(original_UID, add_id, model_id=1027):
    """function generates new UID based on original one. 
    It truncates it if it'll take more than MAX_SERIES_LENGTH, 
    so that final series takes max of MAX_SERIES_LENGTH symbols"""

    original_UID_str = str(original_UID)
    appendix = "." + str(model_id) + "." + str(add_id)
    extra = len(original_UID_str+appendix)-MAX_SERIES_LENGTH

    if extra>0:
        return (original_UID_str[:-extra]+appendix).replace('..','.')
    else:
        return uid(original_UID_str + appendix)


def get_formatted_string(value):
    """Formats a dicom header string properly
    According to http://dicom.nema.org/dicom/2013/output/chtml/part05/sect_6.2.html
    strings need to be an even number of characters. If necessary, pad with a
    single space.
    """
    formatted_string = multi_string(value)
    if len(formatted_string) % 2 != 0:
        formatted_string = formatted_string + ' '
    return formatted_string


def current_time():
    return datetime.datetime.now().astimezone().isoformat(timespec="milliseconds")[:-3] + "00"


def current_date_time():
    return datetime.datetime.now().astimezone(pytz.timezone('Europe/Moscow')).strftime("%Y-%m-%d %H:%M")

