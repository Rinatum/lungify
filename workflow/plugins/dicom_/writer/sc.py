import logging
import os
import time

import cv2
import numpy as np
from pydicom.dataset import Dataset
from PIL import Image

from dicom_.writer.reports import generate_basic_dataset
from dicom_.writer.helpers import current_date_time


def pad_constant(img, border=30, color=(255, 255, 255)):
    return cv2.copyMakeBorder(img,
                              border,
                              border,
                              border,
                              border,
                              borderType=0,
                              value=color)


def write_meta(img, time, color=(0, 0, 255)):
    texted = img.copy()
    cv2.putText(texted, "Только для исследовательских целей", (10, 20), 3, 0.35, color, 1)
    cv2.putText(texted, "AIRadiology v1.2", (10, texted.shape[0] - 10), 3, 0.35, color, 1)
    cv2.putText(texted, time, (texted.shape[1] // 2, texted.shape[0] - 10), 3, 0.35, color, 1)
    return texted


class SC:
    def __init__(self, origin_ds, map_path, proba, series_number="1", THRESHOLD=0.5):
        self.original_UID = origin_ds.SeriesInstanceUID

        modification_time = time.strftime("%H%M%S")
        modification_date = time.strftime("%Y%m%d")

        # File meta info data elements
        file_meta = Dataset()
        file_meta.MediaStorageSOPClassUID = "1.2.840.10008.5.1.4.1.1.7"

        # Main data elements
        ds = generate_basic_dataset(add_id=series_number, origin_ds=origin_ds, original_UID=self.original_UID)
        ds.SOPClassUID = '1.2.840.10008.5.1.4.1.1.7'
        ds.Modality = "SC"

        ds.add_new((0x0008, 0x0000), 'UL', 128)
        ds.add_new((0x0010, 0x0000), 'UL', 18)
        ds.add_new((0x0020, 0x0000), 'UL', 72)
        ds.add_new((0x0028, 0x0000), 'UL', 1368)
        ds.SamplesPerPixel = 1

        # filter_number = origin_ds.get((0x0040, 0x2017))
        # if filter_number:
        #     ds.add_new(0x00402017, "LO", filter_number)
        # else:
        #     logging.info('No Filler Order Number / Imaging Service Request found')

        ds.SeriesDescription = "AIRadiology"
        ds.InstitutionName = "Innopolis University"
        ds.InstitutionalDepartmentName = "0.1"
        ds.AcquisitionDate = modification_date
        ds.AcquisitionTime = modification_time
        ds.OperatorsName = str(proba)
        ds.AdmittingDiagnosesDescription = "ТОЛЬКО ДЛЯ ИССЛЕДОВАТЕЛЬСКИХ ЦЕЛЕЙ"

        logging.info(f"\nThresh : {THRESHOLD} \nPrediction: {proba}")
        if proba < THRESHOLD:
            ds.OperatorsName = "Патологических признаков не выявлено"

        im_frame = Image.open(map_path)  # the PNG file to be replace

        time_ = current_date_time()
        img = np.array(im_frame)
        padded = pad_constant(img)
        texted = write_meta(padded, time_)
        texted = Image.fromarray(texted)

        logging.info(im_frame.mode)

        if im_frame.mode == 'L':
            # (8-bit pixels, black and white)
            np_frame = np.array(texted.getdata(), dtype=np.uint8)
            ds.Rows = texted.height
            ds.Columns = texted.width
            ds.PhotometricInterpretation = "MONOCHROME1"
            ds.SamplesPerPixel = 1
            ds.BitsStored = 8
            ds.BitsAllocated = 8
            ds.HighBit = 7
            ds.PixelRepresentation = 0
            ds.PixelData = np_frame.tobytes()
        elif im_frame.mode == 'RGBA' or im_frame.mode == "RGB":
            # RGBA (4x8-bit pixels, true colour with transparency mask)
            np_frame = np.array(texted.getdata(), dtype=np.uint8)[:, :3]
            ds.Rows = texted.height
            ds.Columns = texted.width
            ds.PhotometricInterpretation = "RGB"
            ds.SamplesPerPixel = 3
            ds.BitsStored = 8
            ds.BitsAllocated = 8
            ds.HighBit = 7
            ds.PixelRepresentation = 0
            ds.PixelData = np_frame.tobytes()

        ds.file_meta = file_meta
        ds.is_implicit_VR = True
        ds.is_little_endian = True
        ds.SpecificCharacterSet = 'ISO_IR 192'
        self.ds = ds

    def save(self, output_dir, name=None):
        if not name:
            name = self.ds.SOPInstanceUID
        # Write to the output directory and add the extension dcm, to force writing in DICOM format.
        dicom_filename = '{0}.dcm'.format(str(name))
        dicom_path = os.path.join(output_dir, dicom_filename)
        self.ds.save_as(dicom_path, write_like_original=False)
        return dicom_path
