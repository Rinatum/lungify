import time

from pydicom import Dataset

from dicom_.writer.helpers import current_date_time, get_formatted_string, generate_UID

TEMPLATES = \
    {
        "Pneumonia":
            {True: """На рентгенограмме в прямой проекции признаки инфильтративных изменений.
        Необходимо согласовать с клиническими и лабораторными данными для дифференциальной диагностики
        Вероятность пневмонии высокая ({})
        """,
             False: """На рентгенограмме в прямой проекции без явных признаков инфильтративных изменений.
        Необходимо согласовать с клиническими и лабораторными данными.
        Вероятность пневмонии низкая ({})
        """},

        "Pathology":
            {True: """На рентгенограмме в прямой проекции признаки патологии легких.
        Необходимо согласовать с клиническими и лабораторными данными для дифференциальной диагностики
        Вероятность патологии высокая ({})
        """,
             False: """На рентгенограмме в прямой проекции без явных признаков патологических изменений.
        Необходимо согласовать с клиническими и лабораторными данными.
        Вероятность патологии низкая ({})
        """}
    }


def create_file_meta(MediaStorageSOPInstanceUID):
    file_meta = Dataset()
    file_meta.FileMetaInformationGroupLength = 174
    file_meta.FileMetaInformationVersion = b'\x00\x01'
    file_meta.MediaStorageSOPClassUID = "1.2.840.10008.5.1.4.1.1.88.33"
    file_meta.MediaStorageSOPInstanceUID = MediaStorageSOPInstanceUID
    file_meta.ImplementationClassUID = "1.2.40.0.13.1.1"
    file_meta.TransferSyntaxUID = "1.2.840.10008.1.2"

    return file_meta


def generate_basic_dataset(origin_ds, original_UID, add_id):
    modification_time = time.strftime("%H%M%S")
    modification_date = time.strftime("%Y%m%d")

    UID = generate_UID(original_UID=original_UID, add_id=add_id)

    ds = Dataset()
    ds.SOPInstanceUID = UID
    ds.SeriesInstanceUID = UID
    ds.StationName = "Mosmed"

    study_date = origin_ds.get("StudyDate")
    ds.StudyDate = study_date if study_date else modification_date
    ds.ContentDate = get_formatted_string(modification_date)

    study_time = origin_ds.get("StudyTime")
    ds.StudyTime = study_time if study_time else get_formatted_string(modification_time)
    ds.ContentTime = get_formatted_string(modification_time)

    study_iuid = origin_ds.get("StudyInstanceUID")
    ds.StudyInstanceUID = study_iuid if study_iuid else generate_UID(original_UID=original_UID, add_id=add_id)

    study_id = origin_ds.get("StudyID")
    ds.StudyID = study_id if study_id else "404"

    accession_number = origin_ds.get("AccessionNumber")
    if accession_number:
        ds.AccessionNumber = accession_number

    patient_id = origin_ds.get("PatientID")
    if patient_id:
        ds.PatientID = patient_id

    patient_name = origin_ds.get("PatientName")
    ds.PatientName = patient_name if patient_name else "Anon"

    issuer = origin_ds.get("IssuerOfPatientID")
    if issuer:
        ds.IssuerOfPatientID = issuer

    filter_ = origin_ds.get("FillerOrderNumberImagingServiceRequest")
    if filter_:
        ds.FillerOrderNumberImagingServiceRequest = filter_

    return ds


def get_report_text(proba, THRESHOLD):
    disease_found = proba > THRESHOLD
    return TEMPLATES["Pathology"][disease_found].format(str(proba))


def generate_sequence_report(origin_ds, proba, THRESHOLD=0.5):
    return [
        Content(CodeMeaning='Название сервиса', TextValue='AIRadiology'),
        Content(CodeMeaning='Предупреждение', TextValue='ТОЛЬКО ДЛЯ ИССЛЕДОВАТЕЛЬСКИХ ЦЕЛЕЙ'),
        Content(CodeMeaning='Версия ПО', TextValue='1.2'),
        Content(CodeMeaning='Дата и время анализа', TextValue=current_date_time()),
        Content(CodeMeaning='Назначение сервиса', TextValue=f'''
                                                               Модальность: {origin_ds.get("Modality", "SC")}
                                                               Анатомическая область: грудная клетка
                                                               Пациенты: взрослое население
                                                               Назначение: поиск признаков патологий ОГК
                                                               Требования к изображениям: прямая проекция ОГК'''),

        Content(CodeMeaning='Краткое руководство пользователя',
                TextValue="Патологические зоны выделены красным цветом переменной интенсивности"),
        Content(CodeMeaning='Заключение', TextValue=get_report_text(proba, THRESHOLD))

    ]


class Content(Dataset):
    def __init__(self, RelationshipType='CONTAINS', ValueType='TEXT',
                 CodeValue='209001', CodingSchemeDesignator='99PMP', CodeMeaning='Название сервиса',
                 TextValue='AI Example'):
        super().__init__()
        self.RelationshipType = RelationshipType
        self.ValueType = ValueType
        self.ConceptNameCodeSequence = [ConceptNameCodeSequence(CodeValue, CodingSchemeDesignator, CodeMeaning)]
        self.TextValue = TextValue


class ConceptNameCodeSequence(Dataset):
    def __init__(self, CodeValue, CodingSchemeDesignator, CodeMeaning):
        super().__init__()
        self.CodeValue = CodeValue
        self.CodingSchemeDesignator = CodingSchemeDesignator
        self.CodeMeaning = CodeMeaning
