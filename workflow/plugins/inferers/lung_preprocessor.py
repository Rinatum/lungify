import logging

import numpy as np
import pydicom

logger = logging.getLogger("airflow.task")


class LungPreprocessor:
    def __init__(self, transform, orientation_classifier, monochrome_classifier):
        self.transform = transform
        self.orientation_classifier = orientation_classifier
        self.monochrome_classifier = monochrome_classifier

    def __call__(self, dicom_paths):
        datasets = [pydicom.read_file(path) for path in dicom_paths]
        preprocessed = [self.transform(dataset) for dataset in datasets]
        logger.info(f"Preprocessed images shapes : {[image.shape for image in preprocessed]}")
        correct_images, correct_ids = self._filter_by_orientation(preprocessed)
        correct_images = self._fix_monochrome(correct_images)
        return correct_images, correct_ids

    def _filter_by_orientation(self, images):
        orientation_probas = self.orientation_classifier.execute(images)['output__0']
        logger.info(f"Orientation Probabilities : {orientation_probas}")

        correct_ids = [i for i, proba in enumerate(orientation_probas) if proba < 0.86]
        correct_images = [images[i] for i in correct_ids]

        return correct_images, correct_ids

    def _fix_monochrome(self, images):
        monochrome_probas = self.monochrome_classifier.execute(images)['output__0']
        logger.info(f"Monochrome Probas : {monochrome_probas}")

        monochrome_predictions = np.array(monochrome_probas).argmax(1)
        logger.info(f"Monochrome Predictions : {monochrome_predictions}")

        return [image if mono == 1 else 255 - image for image, mono in zip(images, monochrome_predictions)]
