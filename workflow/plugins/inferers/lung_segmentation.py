import logging
from typing import List

import cv2
import numpy as np

logger = logging.getLogger("airflow.task")

class LungSegmentation:
    def __init__(self, segmentator):
        self.segmentator = segmentator

    def __call__(self, crops):
        masks = self.segmentator.execute(crops)['output__0']
        masks = self.postprocess_masks(masks)
        return masks


    def postprocess_masks(self, masks_: np.ndarray) -> List[dict]:
        """ Output mask postprocessing:
            - for each of the channels keep only biggest contour on it

            @param: masks_: np.ndarray [N*3*S*S] - predicted mask batch, N - batch size
            @return: List[dict - {"left_lung": [S*S] np.array[bool], "right_lung": [S*S] np.array[bool]}]
        """
        masks = []

        for mask in masks_:
            mask = (mask > 0.5).astype(np.uint8)
            res_dict = {"left_lung": mask[1], "right_lung": mask[2]}

            for key, mask in res_dict.items():

                try:
                    contours, _ = cv2.findContours(
                        mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
                    contour = sorted(
                        contours, key=lambda x: cv2.contourArea(x), reverse=True)[0]
                    mask = np.zeros(mask.shape, dtype=np.float)
                    cv2.drawContours(mask, [contour], 0, 1, -1)
                    mask = mask.astype(np.uint8)
                except:
                    mask = np.zeros(mask.shape, dtype=int)
            masks.append(res_dict)
        return masks

    def merge_masks(self, masks_):
        """merge masks from dict to 1channel image"""
        masks = []
        for mask in masks_:
            mask = (mask["left_lung"] + mask["right_lung"]).clip(0,1)
            masks.append(mask)
        
        return masks