import logging
import traceback
import json
from pydicom import read_file

from confluent_kafka import Producer as KafkaProducer, KafkaError

from airflow.models.dag import Context, TaskInstance
from utils import get_conf_dict, get_resource_id, current_time
from callbacks.base import Callback

logger = logging.getLogger("airflow.task")


class KafkaCallback(Callback):
    def __init__(self, topics, producer_params):
        self.topics = topics
        self.producer_params = producer_params
        
        self.producer = None

    def run(self, context: Context):
        self.producer = KafkaProducer(**self.producer_params)
        messages = self.generate_messages(context)
        for message in messages:
            self.send_kafka_message(message)

    def generate_messages(self, context: Context):
        raise NotImplementedError

    def send_kafka_message(self, data):
        self.producer.poll(0)
        self.producer.produce(self.topics,
                              json.dumps(data).encode('utf-8'),
                              callback=self.kafka_delivery_report)
        self.producer.flush()

    @staticmethod
    def kafka_delivery_report(err, msg):
        """ Called once for each message produced to indicate delivery result.
            Triggered by poll() or flush(). """
        if err is not None:
            logger.error('Kafka message delivery failed: {}'.format(err))
        else:
            logger.info('Kafka message delivered to {} [{}]'.format(msg.topic(), msg.partition()))


class KafkaSuccessCallback(KafkaCallback):
    def generate_messages(self, context):
        conf_dict = get_conf_dict(context)
        time_data = conf_dict["downloadDT"]
        download_start_dt = time_data["downloadStartDT"]
        download_end_dt = time_data["downloadEndDT"]
        task: TaskInstance = context.get('task_instance')
        report_path = task.xcom_pull(key="report_path")
        visualization_path = task.xcom_pull(key="visualization_path")
        final_proba = task.xcom_pull(key="final_proba")
        pathology_flag = final_proba > 0.5
        confidence_level = int(final_proba * 100)
        process_end_dt = current_time()

        report = read_file(report_path,
                           stop_before_pixels=True)
        visualization = read_file(visualization_path,
                                  stop_before_pixels=True)

        study_iuid = report.StudyInstanceUID
        report_series_id = report.SeriesInstanceUID
        visualization_series_id = visualization.SeriesInstanceUID

        report_message = self.generate_message(study_iuid, report_series_id,
                                               pathology_flag, confidence_level,
                                               download_start_dt, download_end_dt, process_end_dt)

        visualization_message = self.generate_message(study_iuid, visualization_series_id,
                                                      pathology_flag, confidence_level,
                                                      download_start_dt, download_end_dt, process_end_dt)

        return [report_message, visualization_message]

    @staticmethod
    def generate_message(study_iuid, series_iuid,
                         pathology_flag, confidence_level,
                         download_start_dt, download_end_dt, process_end_dt):
        message = {
            "studyIUID": study_iuid,
            "aiResult": {
                "seriesIUID": series_iuid,
                "pathologyFlag": pathology_flag,
                "confidenceLevel": confidence_level,
                "modelId": 1027,
                "modelVersion": "0.1",
                "dateTimeParams": {
                    "downloadStartDT": download_start_dt,
                    "downloadEndDT": download_end_dt,
                    "processStartDT": download_end_dt,
                    "processEndDT": process_end_dt
                }
            }
        }

        return message


class KafkaFailureCallback(KafkaCallback):
    def generate_messages(self, context):
        conf_dict = get_conf_dict(context)
        study_iuid = conf_dict["studyTags"]["StudyInstanceUID"]
        time_data = conf_dict["downloadDT"]
        download_start_dt = time_data["downloadStartDT"]
        download_end_dt = time_data["downloadEndDT"]
        exception = context.get("exception")

        error_name = type(exception).__name__
        error_descrption = str(exception)

        message = {
            "studyIUID": study_iuid,
            "aiResult": {
                "modelId": 1027,
                "error": error_name,
                "description": error_descrption,
                "dateTimeParams": {
                    "downloadStartDT": download_start_dt,
                    "downloadEndDT": download_end_dt
                }
            }
        }
        return [message]

