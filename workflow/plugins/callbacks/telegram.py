import os
import re

from airflow.models.dag import Context
from requests.utils import requote_uri
from callbacks.base import Callback
from utils.context import get_resource_id
import requests

from utils.context import get_conf_dict
from exceptions.base import PredefinedException


class TelegramBotCallback(Callback):
    def __init__(self,
                 api_key,
                 chat_id):
        self.api_key = api_key
        self.chat_id = chat_id

    def run(self, context: Context):
        messages = self.generate_messages(context)
        for msg in messages:
            self.send(msg)

    def telegram_get_request(self, method, params):
        url = f"https://api.telegram.org/bot{self.api_key}/{method}"
        return requests.get(url, params=params).json()

    def send(self, message):
        params = {
            "chat_id": self.chat_id,
            "text": message['text']
        }
        if 'parse_mode' in message:
            params.update({"parse_mode": message['parse_mode']})

        resp = self.telegram_get_request("sendMessage", params)
        if resp["ok"]:
            if message["pin"]:
                pin_resp = self.telegram_get_request("pinChatMessage", params={
                    "chat_id": self.chat_id,
                    "message_id": resp["result"]["message_id"],
                    "disable_notification": False,
                })
                if not pin_resp["ok"]:
                    print(f"Error: Telegram bot pin message failed! Response: {pin_resp}")
        else:
            print(f"Error: Telegram bot message sending failed! Response: {resp}")

    @staticmethod
    def format_markdown_message(message):
        message = re.sub("_", "\\_", message)
        message = re.sub("\\*", "\\*", message)
        return message

    @staticmethod
    def generate_messages(context):
        exception = context.get("exception")
        conf_dict = get_conf_dict(context)
        study_id = conf_dict["studyTags"]["StudyInstanceUID"]

        error_message = ""
        pin_message = False
        if issubclass(exception.__class__, PredefinedException):
            error_message += "* Warning ✅ *\n"
        else:
            pin_message = True
            error_message += "* Unexpected Error ❌ *\n"
        url = f'{os.environ.get("PUBLIC_IP", "localhost")}:{os.environ.get("PUBLIC_PORT", "8080")}'
        error_message += f"""
Task: `{context['task'].task_id}`
Resource id: `{get_resource_id(context)}`
Study id: `{study_id}`
Dag run id: `{context['dag_run'].run_id}`
Error name: `{type(exception).__name__}`
Description: `{getattr(exception, 'message', str(exception))}`
""" + f"[Logs]({requote_uri(context['task_instance'].log_url.replace('localhost:8080', url))} )"

        print("Telegram message: ", error_message)

        return [{"text": error_message, "parse_mode": "Markdown", "pin": pin_message}]
