import os
import shutil

from airflow.models.dag import Context
from callbacks.base import Callback

from utils.fs import get_dag_payload_path


class ClearPayloadCallback(Callback):
    def run(self, context: Context):
        payload_path = get_dag_payload_path(context)
        shutil.rmtree(payload_path)


class CopyPayloadCallback(Callback):
    def __init__(self, fail_path):
        self.fail_path = fail_path

    def run(self, context: Context):
        payload_path = get_dag_payload_path(context)
        save_payload_path = os.path.join(self.fail_path, context['dag_run'].run_id)
        os.mkdir(save_payload_path)
        os.system(f"cp -r {payload_path} {save_payload_path}")
