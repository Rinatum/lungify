from exceptions.base import PredefinedException


class EmptyBatch(PredefinedException):
    def __str__(self):
        return "Input Batch is empty"
