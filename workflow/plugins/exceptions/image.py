from exceptions.base import PredefinedException


class CorrectViewPositionNotFound(PredefinedException):
    def __str__(self):
        return "There is no image with correct view position"
