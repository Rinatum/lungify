from airflow.exceptions import AirflowException


class PredefinedException(AirflowException):
    pass
