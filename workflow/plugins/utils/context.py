from airflow.operators.python import get_current_context


def check_context(func):
    def wrapper(context=None):
        return func(get_current_context()) if not context else func(context)
    return wrapper
    

@check_context
def get_conf_dict(context):
    return context["dag_run"].conf


@check_context
def get_resource_id(context):
    return context["dag_run"].conf["resourceId"]


@check_context
def get_metadata(context):
    return context["dag_run"].conf["metadata"]
