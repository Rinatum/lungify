import datetime


def current_time():
    return datetime.datetime.now().astimezone().isoformat(timespec="milliseconds")[:-3] + "00"
