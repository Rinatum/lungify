from typing import Union, Dict

import yaml

from triton.client import TritonImageClient
from triton.structure import TritonImageClientStructure
from transforms import TRANSFORMS


def parse_triton(config: Union[Dict, str]):
    if isinstance(config, str):
        config: dict = yaml.load(open(config, "r"), Loader=yaml.FullLoader)
    params = TritonImageClient.deserialize(config, TRANSFORMS)
    wrapper = TritonImageClient(**params)
    return wrapper
