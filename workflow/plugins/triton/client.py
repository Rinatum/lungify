from dataclasses import dataclass
from typing import Dict, List, Optional

import numpy as np
import tritonhttpclient
from tritonhttpclient import InferInput, InferRequestedOutput

from transforms.base import Transform, ImageTransform
from triton.structure import ConnectorParams, TritonImageClientStructure
from exceptions import EmptyBatch


# from official tritonclient docs.
def triton_to_np_dtype(dtype):
    if dtype == "BOOL":
        return np.bool
    elif dtype == "INT8":
        return np.int8
    elif dtype == "INT16":
        return np.int16
    elif dtype == "INT32":
        return np.int32
    elif dtype == "INT64":
        return np.int64
    elif dtype == "UINT8":
        return np.uint8
    elif dtype == "UINT16":
        return np.uint16
    elif dtype == "UINT32":
        return np.uint32
    elif dtype == "UINT64":
        return np.uint64
    elif dtype == "FP16":
        return np.float16
    elif dtype == "FP32":
        return np.float32
    elif dtype == "FP64":
        return np.float64
    elif dtype == "BYTES":
        return np.object
    return None


@dataclass
class Preprocessor:
    datatype: str
    transform: Optional[Transform] = None


@dataclass
class Postprocessor:
    binary_data: bool = True
    class_count: int = 0
    transform: Optional[Transform] = None


class TritonImageClient:
    config = TritonImageClientStructure

    def __init__(
            self,
            model_name: str,
            model_version: str,
            connector_params: ConnectorParams,
            preprocessors: Dict[str, Preprocessor],
            postprocessors: Dict[str, Postprocessor],
    ):

        self.connector = tritonhttpclient.InferenceServerClient(**connector_params.dict())

        self.model_name = model_name
        self.model_version = model_version

        assert self.connector.is_model_ready(
            model_name, model_version
        ), f"{self.model_name}:v{self.model_version} is not ready!"

        self.preprocessors = preprocessors
        self.postprocessors = postprocessors

        self.outputs = self.prepare_outputs()

    def prepare_inputs(self, images) -> List[InferInput]:
        inputs = []
        for input_name, preprocessor in self.preprocessors.items():
            preprocessed = images.copy()
            if preprocessor.transform:
                preprocessed = [preprocessor.transform(image) for image in images]
            batch = np.stack(preprocessed, axis=0)
            input_ = InferInput(input_name, batch.shape, preprocessor.datatype)
            input_.set_data_from_numpy(batch, binary_data=True)
            inputs.append(input_)

        return inputs

    def prepare_outputs(self) -> List[InferRequestedOutput]:
        outputs = []
        for output_name, postprocessor in self.postprocessors.items():
            output = tritonhttpclient.InferRequestedOutput(
                output_name, postprocessor.binary_data, postprocessor.class_count
            )
            outputs.append(output)

        return outputs

    def predict(self, images) -> dict:
        inputs = self.prepare_inputs(images)

        response = self.connector.infer(
            model_name=self.model_name,
            inputs=inputs,
            model_version=self.model_version,
            outputs=self.outputs,
        )

        prediction = {}

        for output_name, postprocessor in self.postprocessors.items():
            results = list(response.as_numpy(output_name))
            if postprocessor.transform:
                results = [postprocessor.transform(result) for result in results]

            prediction[output_name] = results

        return prediction

    @classmethod
    def deserialize(cls, config: dict, transforms: Dict[str, ImageTransform]):
        structure = cls.config(**config)

        def parse_transforms(transforms_list):
            return transforms["Compose"]([transforms[transform.name](**transform.params)
                                          for transform in transforms_list])

        for name, preprocessor in structure.preprocessors.items():
            if preprocessor.transform:
                preprocessor.transform = parse_transforms(preprocessor.transform)
        for name, postprocessor in structure.postprocessors.items():
            if postprocessor.transform:
                postprocessor.transform = parse_transforms(postprocessor.transform)

        return {"model_name": structure.model_name,
                "model_version": structure.model_version,
                "connector_params": structure.connector_params,
                "preprocessors": structure.preprocessors,
                "postprocessors": structure.postprocessors}

    def execute(self, image_batch: List[np.ndarray]) -> dict:
        if len(image_batch) == 0:
            raise EmptyBatch()
        return self.predict(image_batch)
