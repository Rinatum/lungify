import numpy as np

from transforms.base import Transform
# from registry import TRANSFORMS


# @TRANSFORMS.register_class
class ToStrReport(Transform):
    """
    Convert prediction to correct string format
    Example:
         array([b'0.583699:0:Pathology'] -> 0.583699
    """

    def apply(self, prediction: np.ndarray):
        proba, pred, flag = prediction[0].decode("utf-8").split(":")

        return float(proba)
