from typing import Dict

import yaml

from transforms.base import *
from transforms.mosmed import *
from transforms.dicom import *

TRANSFORMS: dict = {
    k: v
    for k, v in globals().items()
    if isinstance(v, type) and issubclass(v, Transform)
}


def parse_transforms(config: Union[Dict, str]):
    if isinstance(config, str):
        config: dict = yaml.load(open(config, "r"), Loader=yaml.FullLoader)
    return TRANSFORMS["Compose"]([TRANSFORMS[transform["name"]](**transform.get("params", {}))
                                  for transform in config])
