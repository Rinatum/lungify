from pydicom.pixel_data_handlers.util import apply_modality_lut, apply_voi_lut

# from registry import TRANSFORMS
from transforms.base import Transform


# @TRANSFORMS.register_class
class Dicom2Img(Transform):
    def apply(self, dataset):
        # catching error when VOILUTSequence is empty
        try:
            img = apply_modality_lut(apply_voi_lut(dataset.pixel_array, dataset), dataset)
        except IndexError as e:
            img = apply_modality_lut(dataset.pixel_array, dataset)
        return img
