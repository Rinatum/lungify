import cv2
import numpy as np

from transforms.lungs_finder import haar_finder
from transforms.lungs_finder import hog_finder
from transforms.lungs_finder import lbp_finder


def find_max_rectangle(rectangles):
    max_rectangle = None
    max_area = 0

    for rectangle in rectangles:
        x, y, width, height = rectangle
        area = width * height

        if area > max_area:
            max_area = area
            max_rectangle = rectangle

    return max_rectangle


def get_lungs(image):
    right_lung = hog_finder.find_right_lung_hog(image)
    left_lung = hog_finder.find_left_lung_hog(image)

    if right_lung is not None and left_lung is not None:
        x_right, y_right, width_right, height_right = right_lung
        x_left, y_left, width_left, height_left = left_lung

        if abs(x_right - x_left) < min(width_right, width_left):
            right_lung = lbp_finder.find_right_lung_lbp(image)
            left_lung = lbp_finder.find_left_lung_lbp(image)

    if right_lung is None:
        right_lung = haar_finder.find_right_lung_haar(image)

    if left_lung is None:
        left_lung = haar_finder.find_left_lung_haar(image)

    if right_lung is None:
        right_lung = lbp_finder.find_right_lung_lbp(image)

    if left_lung is None:
        left_lung = lbp_finder.find_left_lung_lbp(image)

    if right_lung is None and left_lung is None:
        return None
    elif right_lung is None:
        x, y, width, height = left_lung
        spine = width / 5
        right_lung = int(x - width - spine), y, width, height
    elif left_lung is None:
        x, y, width, height = right_lung
        spine = width / 5
        left_lung = int(x + width + spine), y, width, height

    x_right, y_right, _, height_right = right_lung
    x_left, y_left, width_left, height_left = left_lung

    if x_right < 0:
        x_right = 0

    if y_right < 0:
        y_right = 0

    if x_left < 0:
        x_left = 0

    if y_left < 0:
        y_left = 0

    if y_right + height_right > image.shape[0]:
        height_right = image.shape[0] - y_right

    if x_left + width_left > image.shape[1]:
        width_left = image.shape[1] - x_left

    if y_left + height_left > image.shape[0]:
        height_left = image.shape[0] - y_left

    top_y = min(y_right, y_left)
    bottom_y = max(y_right + height_right, y_left + height_left)
    if image[top_y:bottom_y, x_right:x_left + width_left] is not None:
        w, h = np.shape(image[top_y:bottom_y, x_right:x_left + width_left])
        return x_right, top_y, h, w
    else:
        return None
    # return image[top_y:bottom_y, x_right:x_left + width_left]


def get_lung_coords(image, indent=10):
    """
    crops lungs on image and returns it
    Args:
        image: numpy (may be works with tensors?) array image. Can be grayscale or colored
        indent: int - how much pixels to add aroung lungs
        return_coords: bool - set to true to return coords (x_min, y_min, x_max, y_max) instead of cropped image. Coords are relative, not absolute (i.e between 0 and 1)
    """

    # img shape
    iw, ih = image.shape[:2]

    # getting BBoxes for lungs
    if image.ndim == 3:
        lungs = get_lungs(cv2.cvtColor(image, cv2.COLOR_RGB2GRAY))
    else:
        lungs = get_lungs(image)

    if lungs is not None:
        # unpacking
        x, y, w, h = lungs

        # lungs always take more than 59 percent of width? No idea wth is this
        if w > iw * 0.59 and h > ih * 0.50:
            # check whether the bounding rectangle extends beyond the image
            if x - indent < 0 or y - indent < 0 or x + w + indent > iw or y + h + indent > ih:
                # compute an optimal indent from bounding rectangle
                indent -= max((lambda: 0 if x > indent else indent - x)(),
                              (lambda: 0 if y > indent else indent - y)(),
                              (
                                  lambda: 0 if x + w + indent < iw else x + w + indent - iw)(),
                              (
                                  lambda: 0 if y + h + indent < ih else y + h + indent - ih)())

            # compute minimum upper left x y coordinates and maximum lower right x y coordinates.
            x_min = x - indent
            y_min = y - indent
            x_max = x + w + indent
            y_max = y + h + indent

            return x_min, y_min, x_max, y_max

    # if nothing is found, we return entire image
    return 0, 0, iw, ih


def to_relative_coords(image, x_min, y_min, x_max, y_max):
    iw, ih = image.shape[:2]
    return x_min / iw, y_min / ih, x_max / iw, y_max / ih


def to_absolute_coords(image, coords):
    sp = image.shape[:2]
    xmax = int(coords[2] * sp[0])
    xmin = int(coords[0] * sp[0])
    ymax = int(coords[3] * sp[1])
    ymin = int(coords[1] * sp[1])

    return xmin, ymin, xmax, ymax


def crop(img, x_min, y_min, x_max, y_max):
    height, width = img.shape[:2]
    if x_max <= x_min or y_max <= y_min:
        raise ValueError(
            "We should have x_min < x_max and y_min < y_max. But we got"
            " (x_min = {x_min}, y_min = {y_min}, x_max = {x_max}, y_max = {y_max})".format(
                x_min=x_min, x_max=x_max, y_min=y_min, y_max=y_max
            )
        )

    if x_min < 0 or x_max > width or y_min < 0 or y_max > height:
        raise ValueError(
            "Values for crop should be non negative and equal or smaller than image sizes"
            "(x_min = {x_min}, y_min = {y_min}, x_max = {x_max}, y_max = {y_max}, "
            "height = {height}, width = {width})".format(
                x_min=x_min, x_max=x_max, y_min=y_min, y_max=y_max, height=height, width=width
            )
        )

    return img[y_min:y_max, x_min:x_max]
