from typing import List, Union
from json import dumps

import cv2
import numpy as np
import torch
import pydicom
from albumentations import functional as F
from matplotlib import pyplot as plt

# from registry import TRANSFORMS
from transforms.lungs_finder import get_lung_coords, crop


class Transform:
    def __init__(self, **params):
        self.params = params

    def __call__(self, *args, **kwargs):
        return self.apply(*args, **kwargs)

    def apply(self, *args, **kwargs):
        return NotImplementedError

    def __str__(self):
        return f"{self.__class__.__name__}\n{dumps(self.params, indent=4)}"


class ImageTransform(Transform):
    def __call__(self, image: np.ndarray) -> np.ndarray:
        return self.apply(image)

    def apply(self, image: np.ndarray) -> np.ndarray:
        raise NotImplementedError

    def _check(self, fields: List[str]):
        for field in fields:
            assert field in self.params, f"{field} is required field"


# @TRANSFORMS.register_class
class Compose(Transform):
    def __init__(self, transforms: List[Transform]):
        for transform in transforms:
            assert isinstance(transform, Transform), f"{type(transform)} is not subclass of {Transform}"
        super().__init__(transforms=transforms)

    def apply(self, item: Union[np.ndarray, pydicom.Dataset]):
        if isinstance(item, np.ndarray):
            output = item.copy()
        elif isinstance(item, pydicom.Dataset):
            output = item  # TODO: Create a copy of dataset
        else:
            raise TypeError(f"Type {type(item)} is not allowed")
        for transform in self.params["transforms"]:
            output = transform(output)

        return output

    def __str__(self):
        return "Compose  \n  " + "\n  ".join([str(transform)
                                              for transform in self.params["transforms"]])


# @TRANSFORMS.register_class
class ToRange(ImageTransform):
    """
    Convert [0;255] input to [-range;range] input
    params:
        channels: if 1 - then image will be transformed to 1 channel (just cutting the first channel). In all other cases (even if not passed) it will remain 3 channels
        range - range for image to be (see class description)
    """

    def apply(self, image: np.ndarray):
        self._check(["range"])
        assert np.min(image) >= 0
        assert np.max(image) <= 255

        # if image is [0,1] then we need to do it 0,255 in order for normalizer to work
        if np.max(image) <= 1:
            image *= 255

        return ((image / 127.5 - 1) * self.params["range"]).astype(np.float32)


# @TRANSFORMS.register_class
class Resize(ImageTransform):
    def apply(self, image: np.ndarray):
        self._check(["height", "width"])
        return cv2.resize(image, (self.params["width"], self.params["height"]))


# @TRANSFORMS.register_class
class ToChannelFirst(ImageTransform):
    def apply(self, image: np.ndarray):
        return image.transpose(2, 0, 1)


# @TRANSFORMS.register_class
class ToChannelLast(ImageTransform):
    def apply(self, image: np.ndarray):
        return image.transpose(1, 2, 0)


# @TRANSFORMS.register_class
class Unsqueeze(ImageTransform):
    def apply(self, image: np.ndarray):
        return np.expand_dims(image, axis=self.params["axis"])


# @TRANSFORMS.register_class
class CopyChannels(ImageTransform):
    def apply(self, image: np.ndarray):
        return cv2.merge([image] * self.params["n_chans"])


# @TRANSFORMS.register_class
class Abs(ImageTransform):
    def apply(self, image: np.ndarray):
        return np.abs(image)


# @TRANSFORMS.register_class
class ToUint8(ImageTransform):
    def apply(self, image: np.ndarray):
        dtype = image.dtype

        if np.issubdtype(dtype, np.uint8):
            return image

        if np.issubdtype(dtype, np.integer):
            image = image / np.iinfo(dtype).max

        if np.issubdtype(dtype, np.floating):
            max_val = image.max()

            if max_val <= 1 + 1e-7:
                return (image * 255).astype("uint8")

            elif max_val <= 255 + 1e-7:
                return image.astype("uint8")

            else:
                return (image / 65355. * 255).astype("uint8")


# @TRANSFORMS.register_class
class ToTensorV2(ImageTransform):
    def apply(self, image: np.ndarray) -> torch.Tensor:
        output = image.copy()
        if len(output.shape) == 2:
            output = np.expand_dims(output, -1)
        return torch.from_numpy(output.transpose(2, 0, 1))


# @TRANSFORMS.register_class
class ToGray(ImageTransform):
    def apply(self, image: np.ndarray):
        if len(image.shape) == 2:
            return np.expand_dims(image, -1)
        else:
            return cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)


# @TRANSFORMS.register_class
class Normalize(ImageTransform):
    def __init__(self, mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225), max_pixel_value=255.0):
        super().__init__()
        self.mean = mean
        self.std = std
        self.max_pixel_value = max_pixel_value

    def apply(self, image: np.ndarray):
        return F.normalize(image, self.mean, self.std, self.max_pixel_value)


# @TRANSFORMS.register_class
class MinMaxNormalize(ImageTransform):
    def apply(self, image: np.ndarray):
        image -= image.min()
        image /= (image.max() + 1e-7)

        return image


# @TRANSFORMS.register_class
class ChangeCmap(ImageTransform):
    def __init__(self, cmap):
        super().__init__()
        self.cmap = plt.get_cmap(cmap)

    def apply(self, image: np.ndarray):
        return self.cmap(image)[:, :, :3]


# @TRANSFORMS.register_class
class LungCrop(ImageTransform):
    def apply(self, image: np.ndarray):
        x_min, y_min, x_max, y_max = get_lung_coords(image)
        cropped = crop(image, x_min, y_min, x_max, y_max)
        return cropped
