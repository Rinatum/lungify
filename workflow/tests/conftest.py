import os
import pytest

from airflow.models import DagBag

DAGS_FOLDER = "/opt/airflow/tests"


@pytest.fixture(scope="session")
def dag_bag():
    return DagBag(dag_folder=DAGS_FOLDER, include_examples=False)