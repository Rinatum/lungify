def test_dag_is_correct(dag_bag):
    dag = dag_bag.get_dag(dag_id='dummy')
    assert dag_bag.import_errors == {}
    assert dag is not None
    assert len(dag.tasks) == 3
