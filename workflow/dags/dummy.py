import logging
from typing import Union

from airflow.decorators import dag, task
from airflow.utils.dates import days_ago

from utils import get_unique_dag_id, get_conf_dict

default_args = {
    'owner': 'airflow'
}

logger = logging.getLogger("airflow.task")


@dag(dag_id=get_unique_dag_id(__file__), default_args=default_args, schedule_interval=None, start_date=days_ago(2))
def simple_summator():
    @task()
    def product_a() -> Union[int, float]:
        conf = get_conf_dict()
        multiplier1, multiplier2 = conf["a"]

        return multiplier1 * multiplier2

    @task()
    def division_b() -> float:
        conf = get_conf_dict()
        numerator, denominator = conf["b"]

        return numerator / denominator

    @task()
    def final_sum(product: float, division: Union[int, float]) -> float:
        return product + division

    a = product_a()
    b = division_b()
    final_sum(a, b)


summator = simple_summator()
