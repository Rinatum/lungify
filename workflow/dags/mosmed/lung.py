import logging
import os
from subprocess import CalledProcessError
from typing import List, Dict

import cv2
import yaml
import pydicom
import numpy as np
from airflow.decorators import dag, task
from airflow.utils.dates import days_ago

from triton import parse_triton
from transforms import parse_transforms
from transforms.lungs_finder import get_lung_coords, to_relative_coords, to_absolute_coords, crop
from dicom_.reader import filter_lung_dicom_dir
from dicom_.writer import SR, SC
from dicom_.scu import CliAE
from inferers import LungPreprocessor, LungSegmentation
from utils import get_task_payloads_path, get_metadata, get_conf_dict, get_unique_dag_id, \
    get_current_dir_path, get_dag_payload_path
from callbacks import CallbackList, ClearPayloadCallback, CopyPayloadCallback, \
    KafkaSuccessCallback, KafkaFailureCallback, TelegramBotCallback

from exceptions import CorrectViewPositionNotFound, EmptyBatch

DAG_PATH = get_current_dir_path(__file__)

CONFIG_PATH = os.path.join("/configs", os.environ["CONFIG_NAME"])
CONFIG = yaml.load(open(CONFIG_PATH, "r"), Loader=yaml.FullLoader)
DICOM_MAPPINGS = CONFIG["dicom_destinations"]

CALLBACKS = CONFIG["callbacks"]
KAFKA_PARAMS = CALLBACKS["kafka"]
KAFKA_SUCCESS = KAFKA_PARAMS["success"]
KAFKA_FAIL = KAFKA_PARAMS["fail"]

PAYLOAD_PARAMS = CALLBACKS["payload"]
TELEGRAM_PARAMS = CALLBACKS["telegram"]

# These args will get passed on to each operator
# You can override them on a per-task basis during operator initialization
default_args = {
    'owner': 'airflow',
    'on_failure_callback': CallbackList(
        [
            KafkaFailureCallback(**KAFKA_FAIL),
            TelegramBotCallback(**TELEGRAM_PARAMS),
            CopyPayloadCallback(**PAYLOAD_PARAMS),
            ClearPayloadCallback()
        ]
    )
}

logger = logging.getLogger("airflow.task")


@dag(dag_id=get_unique_dag_id(__file__), default_args=default_args, schedule_interval=None, start_date=days_ago(2))
def lung_mosmed():
    @task()
    def filter_by_dicom_fields() -> List[str]:
        input_dicom_dir = os.path.join(get_dag_payload_path(), "IMAGES")
        dicoms = filter_lung_dicom_dir(input_dicom_dir)

        if len(dicoms) == 0:
            raise CorrectViewPositionNotFound()

        return dicoms

    @task(multiple_outputs=True)
    def preprocess(filtered_paths: List[str]) -> Dict[str, List[str]]:
        dicom2img = parse_transforms(CONFIG["transforms"]["dicom2img"])
        orientation_classifier = parse_triton(CONFIG["triton"]["orientation_classifier"])
        monochrome_classifier = parse_triton(CONFIG["triton"]["monochrome_classifier"])

        preprocessor = LungPreprocessor(transform=dicom2img,
                                        orientation_classifier=orientation_classifier,
                                        monochrome_classifier=monochrome_classifier)

        try:
            correct_images, correct_ids = preprocessor(filtered_paths)
        except EmptyBatch:
            raise CorrectViewPositionNotFound()

        correct_oriented_paths = [path for path in filtered_paths]

        preprocessed_dir = get_task_payloads_path()
        inverted_paths = []
        for i, image in enumerate(correct_images):
            inverted_path = os.path.join(preprocessed_dir, f"{i}.jpg")
            cv2.imwrite(inverted_path, image)
            inverted_paths.append(inverted_path)

        return {"inverted_paths": inverted_paths,
                "correct_orientated_paths": correct_oriented_paths}

    @task()
    def lung_crop(preprocessed_paths: List[str]):
        preprocessed = [cv2.imread(path, 0) for path in preprocessed_paths]
        lung_preprocess = parse_transforms(CONFIG["transforms"]["preprocess_lung"])
        preprocessed = [lung_preprocess(image) for image in preprocessed]
        crops_dir = get_task_payloads_path()
        coords = []
        crops = []
        for image in preprocessed:
            x_min, y_min, x_max, y_max = get_lung_coords(image)
            crops.append(crop(image, x_min, y_min, x_max, y_max))
            coords.append(to_relative_coords(image, x_min, y_min, x_max, y_max))

        crops_path = os.path.join(crops_dir, "crops")
        os.mkdir(crops_path)
        coords_path = os.path.join(crops_dir, "coords.npy")

        for i, cr in enumerate(crops):
            cv2.imwrite(os.path.join(crops_path, f"{i}.png"), cr)

        np.save(coords_path, coords, allow_pickle=True)

        return {"crops_path": crops_path, "coords_path": coords_path}

    @task()
    def lung_segmentation(crops_data):
        crops = []
        for f in os.listdir(crops_data["crops_path"]):
            crops.append(cv2.imread(os.path.join(
                crops_data["crops_path"], f), 0))

        lung_segmentator = parse_triton(CONFIG["triton"]["lung_segmentation"])
        segmentator = LungSegmentation(lung_segmentator)
        masks = segmentator(crops)
        masks = segmentator.merge_masks(masks)

        task_dir = get_task_payloads_path()
        masks_path = os.path.join(task_dir, 'masks')
        os.mkdir(masks_path)

        segmentation_path = os.path.join(task_dir, "masks.npy")
        np.save(segmentation_path, masks)

        # for i, mask in enumerate(masks):
        #     cv2.imwrite(os.path.join(
        #         masks_path, f"{i}.png"), mask*255)

        return {"segmentation_path": segmentation_path}

    @task()
    def classify_pathology(crops_data):
        crops = []
        for f in os.listdir(crops_data["crops_path"]):
            crops.append(cv2.imread(os.path.join(crops_data["crops_path"], f), 0))

        pathology_classifier = parse_triton(CONFIG["triton"]["pathology_classifier"])
        predicted = pathology_classifier.execute(crops)

        classified_dir = get_task_payloads_path()

        predictions_path = os.path.join(classified_dir, "prediction.npy")
        visualizations_path = os.path.join(classified_dir, "visualization.npy")

        np.save(predictions_path, predicted["tf_op_layer_strided_slice_5"])
        np.save(visualizations_path, predicted["tf_op_layer_Mul_3"])

        return {"predictions_path": predictions_path, "visualizations_path": visualizations_path}

    @task()
    def visualize_pathology(preprocessed_paths, crops_data, lung_masks_data, pathology_data):
        images = [cv2.imread(path, 0) for path in preprocessed_paths]
        coords = np.load(crops_data["coords_path"], allow_pickle=True)
        lung_masks = np.load(
            lung_masks_data['segmentation_path'], allow_pickle=True)
        out_probas = np.load(
            pathology_data["predictions_path"], allow_pickle=True)
        out_masks = np.load(
            pathology_data["visualizations_path"], allow_pickle=True)

        visualized = []
        for image, coord, mask, out_proba, out_mask in zip(images, coords, lung_masks, out_probas, out_masks):
            xmin, ymin, xmax, ymax = to_absolute_coords(out_mask, coord)
            img_filtered = np.zeros(out_mask.shape, dtype=np.uint8)
            mask = cv2.resize(mask, (256, 256),
                              interpolation=cv2.INTER_NEAREST)
            out_mask[mask == 0] = 0
            img_filtered[ymin:ymax, xmin:xmax] = cv2.resize(
                out_mask, (xmax - xmin, ymax - ymin))
            img_rgb = cv2.cvtColor(cv2.resize(
                image, (256, 256)), cv2.COLOR_GRAY2RGB)
            if out_proba > 0.5:
                img_rgb = ((img_rgb / 255. + img_filtered /
                           255.).clip(0, 1) * 255).astype(np.uint8)

            visualized.append(img_rgb)

        visualized_dir = get_task_payloads_path()

        visualized_paths = []
        for i, image in enumerate(visualized):
            path = os.path.join(visualized_dir, f"{i}.jpg")
            cv2.imwrite(path, image[..., ::-1])
            visualized_paths.append(path)

        return visualized_paths

    @task(multiple_outputs=True)
    def write_dicom(preprocessed_paths, pathology_data, visualized_paths):
        out_probas = np.load(pathology_data["predictions_path"], allow_pickle=True)
        argmax = out_probas.argmax()

        origin_ds_path = preprocessed_paths[argmax]
        final_proba = out_probas[argmax]
        final_map = visualized_paths[argmax]

        origin_ds = pydicom.read_file(origin_ds_path, stop_before_pixels=True)

        sr = SR(origin_ds, final_proba)
        sc = SC(origin_ds, final_map, final_proba)

        output_dicom_dir = get_task_payloads_path()

        report_path = sr.save(output_dicom_dir, "report")
        visualization_path = sc.save(output_dicom_dir, "visualization")

        return {
            "report_path": report_path,
            "visualization_path": visualization_path,
            "final_proba": final_proba,
        }

    @task(on_success_callback=CallbackList(
        [
            KafkaSuccessCallback(**KAFKA_SUCCESS),
            ClearPayloadCallback()
        ]
    )
    )
    def send_results(report_path: str,
                     visualization_path: str,
                     final_proba: float):

        called_aet = get_metadata()["CalledAET"]
        destinations = DICOM_MAPPINGS[called_aet]
        for destination in destinations:
            client = CliAE(**destination)
            try:
                result = client.store(report_path)
                result = client.store(visualization_path)

            except CalledProcessError as exc:
                print("Status : FAIL", exc.returncode, exc.output)
            else:
                print("Output: \n{}\n".format(result))

    filtered = filter_by_dicom_fields()
    preprocessed_data = preprocess(filtered)
    crops_data = lung_crop(preprocessed_data["inverted_paths"])
    lung_masks_data = lung_segmentation(crops_data)
    pathology_data = classify_pathology(crops_data)
    visualized_paths = visualize_pathology(
        preprocessed_data["inverted_paths"], crops_data, lung_masks_data, pathology_data)
    write_task = write_dicom(preprocessed_data["correct_orientated_paths"], pathology_data, visualized_paths)
    send_task = send_results(write_task["report_path"],
                             write_task["visualization_path"],
                             write_task["final_proba"])
    write_task >> send_task


mosmed = lung_mosmed()
