from threading import Thread
import datetime
import orthanc
import zipfile
import urllib.request
import urllib.parse
import base64
import json
import time
import os

ae2dag = {
    "Lungify": "master_mosmed_lung",
    "LungifyDev": "develop_mosmed_lung",
    "LungifyTest": "test_mosmed_lung",
}

print(f"Ae2Dag mappings\n : {json.dumps(ae2dag, indent=4)}")

volume_path = "/shared"
AIRFLOW_USERNAME = os.environ.get("AIRFLOW_USERNAME")
AIRFLOW_PASSWORD = os.environ.get("AIRFLOW_PASSWORD")
AIRFLOW_HOST = os.environ.get("AIRFLOW_HOST")
AIRFLOW_PORT = os.environ.get("AIRFLOW_PORT")
ORTHANC_WEB_PORT = os.environ.get("ORTHANC_WEB_PORT")
DELAY_BEFORE_DELETE = float(os.environ.get("DELAY_BEFORE_DELETE"))

print(f"ORTHANC WEB PORT : {ORTHANC_WEB_PORT}")

TOKEN = orthanc.GenerateRestApiAuthorizationToken()

studies_to_predict = {}


# Waits ${delay} seconds and then executes ${func}
def run_after_delay(delay: float):
    def decorator(func):
        def wrapper(*args, **kwargs):
            def delayed_exec(sec):
                time.sleep(sec)
                func(*args, **kwargs)
            thread = Thread(target=delayed_exec, args=(delay,))
            thread.start()
        return wrapper
    return decorator


def current_time():
    return datetime.datetime.now().astimezone().isoformat(timespec="milliseconds")[:-3] + "00"


@run_after_delay(DELAY_BEFORE_DELETE)
def delete_study(resourceId):
    resp = orthanc_api_get_request(f"http://localhost:8042/studies/{resourceId}", get_method='DELETE')
    return resp


# TODO: empty series, studies
def expand_aet(resourceId):
    study_info = orthanc_api_get_request(f'http://localhost:{ORTHANC_WEB_PORT}/studies/{resourceId}')
    series_resource_id = study_info["Series"][0]
    series_info = orthanc_api_get_request(f'http://localhost:{ORTHANC_WEB_PORT}/series/{series_resource_id}')
    instance_resource_id = series_info["Instances"][0]
    instance_metadata = orthanc_api_get_request(
        f'http://localhost:{ORTHANC_WEB_PORT}/instances/{instance_resource_id}/metadata?expand')
    return instance_metadata


def orthanc_api_get_request(url, get_method='GET'):
    req = urllib.request.Request(url)
    req.add_header('Authorization', TOKEN)
    req.get_method = lambda: get_method
    with urllib.request.urlopen(req) as response:
        content = response.read()
    json_response = json.loads(content)
    return json_response


def save_study(resourceId):
    req = urllib.request.Request(f'http://localhost:{ORTHANC_WEB_PORT}/studies/{resourceId}/media')
    req.add_header('Authorization', TOKEN)
    path_to_zip_file = f'{resourceId}.zip'
    zip_path = os.path.join(volume_path, path_to_zip_file)
    with urllib.request.urlopen(req) as dl_file:
        with open(zip_path, 'wb') as out_file:
            out_file.write(dl_file.read())

    out_dir_path = os.path.join(volume_path, resourceId)
    unzip(zip_path, out_dir_path)


def unzip(path_to_zip_file, directory_to_extract_to, rm_origin_zip=True):
    print(f"UNZIP DICOM DIR TO : {directory_to_extract_to}")
    with zipfile.ZipFile(path_to_zip_file, 'r') as zip_ref:
        zip_ref.extractall(directory_to_extract_to)

    if rm_origin_zip:
        os.remove(path_to_zip_file)

    os.system(f"chmod 777 -R {directory_to_extract_to}")

    return directory_to_extract_to


def get_study_info(resourceId):
    return orthanc_api_get_request(f'http://localhost:{ORTHANC_WEB_PORT}/studies/{resourceId}')


def trigger_airflow(resourceId):
    auth_b64_code = base64.b64encode(f"{AIRFLOW_USERNAME}:{AIRFLOW_PASSWORD}".encode("utf-8")).decode()

    metadata = expand_aet(resourceId)

    called_aet = metadata["CalledAET"]
    dag_id = ae2dag[called_aet]

    datetimes = studies_to_predict.pop(resourceId)
    datetimes["downloadEndDT"] = current_time()

    data = {
        "conf": {"resourceId": resourceId,
                 "metadata": metadata,
                 "downloadDT": datetimes,
                 "studyTags": get_study_info(resourceId)["MainDicomTags"]}
    }

    json_data = json.dumps(data)
    json_as_bytes = json_data.encode('utf-8')

    req = urllib.request.Request(f'http://{AIRFLOW_HOST}:{AIRFLOW_PORT}/api/v1/dags/{dag_id}/dagRuns')
    req.add_header("Content-type", "application/json")
    req.add_header("Authorization", "Basic %s" % auth_b64_code)
    req.add_header('Content-Length', len(json_as_bytes))
    req.add_header("Accept", "application/json")

    resp = urllib.request.urlopen(req, json_as_bytes)
    print("RESPONSE: ", json.load(resp))


def OnChange(changeType, level, resourceId):
    if changeType == orthanc.ChangeType.NEW_STUDY:
        studies_to_predict[resourceId] = {"downloadStartDT": current_time()}

    if resourceId in studies_to_predict and changeType == orthanc.ChangeType.STABLE_STUDY:
        print("STABLE STUDY RECEIVED")
        save_study(resourceId)
        trigger_airflow(resourceId)
        delete_study(resourceId)


orthanc.RegisterOnChangeCallback(OnChange)
